package main;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;

import model.*;

public class Main {

	private static final Logger log = Logger.getLogger(Main.class);

	@PersistenceUnit(unitName = "westbahn")
	private static EntityManagerFactory sessionFactory;

	@PersistenceContext
	private static EntityManager entitymanager;
	
	static SimpleDateFormat dateForm = new SimpleDateFormat("dd.MM.yyyy");
	static SimpleDateFormat timeForm = new SimpleDateFormat("dd.MM.yyyy mm:hh");

	private Main() {
		super();
	}

	public static void main(String[] args) {
		
		BasicConfigurator.configure();
		
		try {
			log.info("Starting \"Mapping Perstistent Classes and Associations\" (task1)");
			sessionFactory = Persistence.createEntityManagerFactory("westbahn");
			entitymanager = sessionFactory.createEntityManager();
			fillDB(entitymanager);
			task01();
			log.info("Starting \"Working with JPA-QL and the Hibernate Criteria API\" (task2)");
			//log.setLevel(Level.OFF);
			task02();
			task02a();
			task02b();
			task02c();
			log.setLevel(Level.ALL);
			task03(entitymanager);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (entitymanager.getTransaction().isActive())
				entitymanager.getTransaction().rollback();
			entitymanager.close();
			sessionFactory.close();
		}
	}

	public static void fillDB(EntityManager em) throws ParseException {
		em.getTransaction().begin();
		// Dummy-Datum
		Date date = Date.valueOf("2017-11-15");

		// Bahnhöfe persistieren
		List<Bahnhof> list = new ArrayList<Bahnhof>();
		list.add(new Bahnhof("WienHbf", 0, 0, 0, true));
		list.add(new Bahnhof("SalzburgHbf", 20, 60, 120, true));
		list.add(new Bahnhof("Amstetten", 40, 124, 169, false));
		list.add(new Bahnhof("Linz-Ost", 140, 320, 250, false));
		list.add(new Bahnhof("Huetteldorf", 3, 5, 19, false));
		list.add(new Bahnhof("Wels-Zentrum", 102, 400, 250, true));
		for (Bahnhof b : list)
			em.persist(b);

		List<Benutzer> listBenutzer = new ArrayList<>();
		listBenutzer.add(new Benutzer("Kacper", "Urbaniec", "kurbaniec@student.tgm.ac.at", "1234", "+43 1234", 0L));
		listBenutzer.add(new Benutzer("Martin", "Wustinger", "mwustinger@student.tgm.ac.at", "1234", "+43 4321", 0L));
		listBenutzer.add(new Benutzer("Manuel", "Kisser", "mkisser@student.tgm.ac.at", "1234", "+43 1234", 0L));
		listBenutzer.add(new Benutzer("Moritz", "Welsch", "mwelsch@student.tgm.ac.at", "1234", "+43 4321", 0L));
		listBenutzer.add(new Benutzer("Sarah", "Breit", "sbreit@student.tgm.ac.at", "1234", "+43 1234", 0L));
		listBenutzer.add(new Benutzer("Fabio", "Fuchs", "ffuchs@student.tgm.ac.at", "1234", "+43 4321", 0L));
		for (Benutzer b : listBenutzer)
			em.persist(b);


		List<Zug> listZug = new ArrayList<>();
		listZug.add(new Zug(date, 100, 10, 10, list.get(0), list.get(1)));
		for (Zug z : listZug)
			em.persist(z);

		List<Strecke> listStrecke = new ArrayList<>();
		listStrecke.add(new Strecke(list.get(0), list.get(1)));
		listStrecke.add(new Strecke(list.get(0), list.get(2)));
		for (Strecke s : listStrecke)
			em.persist(s);

		List<Reservierung> listReservierung = new ArrayList<>();
		listReservierung.add(new Reservierung(date, listZug.get(0), listStrecke.get(0), listBenutzer.get(0)));
		//listReservierung.add(new Reservierung());
		for (Reservierung r: listReservierung)
			em.persist(r);

		List<Zeitkarte> listZeitkarte = new ArrayList<>();
		listZeitkarte.add(new Zeitkarte(date, ZeitkartenTyp.MONATSKARTE));
		for (Ticket t : listZeitkarte) {
			em.persist(t);
		}

		List<Ticket> listTicket = new ArrayList<>();
		listTicket.add(new Zeitkarte(listStrecke.get(1), date, ZeitkartenTyp.JAHRESKARTE));
		listTicket.add(new Zeitkarte(listStrecke.get(1), date, ZeitkartenTyp.JAHRESKARTE));
		for (Ticket t : listTicket) {
			em.persist(t);
		}

		listBenutzer.get(0).setTickets(new Ticket[]{listZeitkarte.get(0)});
		em.merge(listBenutzer.get(0));


		//em.getTransaction().commit();
		em.flush();
	}

	public static void task01() throws ParseException, InterruptedException {
	}

	public static <T> void task02() throws ParseException {
		Query q = entitymanager.createNamedQuery("Bahnhof.getAll");

		List<?> l = q.getResultList();

		for (Object b : l) {
			Bahnhof bhf = null;
			if (b instanceof Bahnhof) {
				bhf = (Bahnhof) b;
				System.out.println("Bahnhof: " + bhf.getName());
			}
		}
	}


	public static void task02a() throws ParseException {
		log.setLevel(Level.INFO);
		Query q = entitymanager.createNamedQuery("Benutzer.getEmail");

		List<?> l = q.getResultList();

		for (Object b : l) {
			String bhf = null;
			if (b instanceof String) {
				bhf = (String) b;
				System.out.println("Email: " + bhf);
			}
		}

		Query q2 = entitymanager.createNamedQuery("Benutzer.getReservierungen");
		q2.setParameter("email", "kurbaniec@student.tgm.ac.at");
		List<?> l2 = q2.getResultList();
		log.info("#####");
		log.info("Reservierungen von \"kurbaniec@student.tgm.ac.at\"");
		for(Object r: l2) {
			log.info(r);
		}
		log.info("#####");
	}

	public static void task02b() throws ParseException {
		Query q = entitymanager.createNamedQuery("Benutzer.getMonatskarten");
		//Query q = entitymanager.createNamedQuery("Zeitkarte.getTyp");
		//q2.setParameter("email", "kurbaniec@student.tgm.ac.at");
		List<?> l = q.getResultList();
		log.info("#####");
		log.info("Monatskarten von Benutzern");
		for(Object r: l) {
			log.info(r);
		}
		log.info("#####");
	}

	public static void task02c() throws ParseException {


		Query q = entitymanager.createNamedQuery("Strecke.getNoReservierungen");
		//Query q = entitymanager.createNamedQuery("Zeitkarte.getTyp");
		//q2.setParameter("email", "kurbaniec@student.tgm.ac.at");
		List<?> l = q.getResultList();
		log.info("#####");
		log.info("Tickets fuer Strecken ohne Reservierungen");
		for(Object r: l) {
			log.info(r);
		}
		log.info("#####");
	}

	public static void task03(EntityManager em) {
	}

	public static void validate(Object obj) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> violations = validator.validate(obj);
		for (ConstraintViolation<Object> violation : violations) {
			log.error(violation.getMessage());
			System.out.println(violation.getMessage());
		}
	}
}

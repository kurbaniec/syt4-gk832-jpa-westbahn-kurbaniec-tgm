package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.util.Date;
import java.util.Objects;

@Entity
@NamedQueries({
	@NamedQuery(name="Zeitkarte.getAll",query="SELECT b from Zeitkarte b"),
	@NamedQuery(name="Zeitkarte.getTyp",query="SELECT b.typ from Zeitkarte b"),
})
public class Zeitkarte extends Ticket {

	private Date gueltigAb;

	private ZeitkartenTyp typ;

	public Zeitkarte(Long ID, Date gueltigAb) {
		super(ID);
		this.gueltigAb = gueltigAb;
	}

	public Zeitkarte(Date gueltigAb, ZeitkartenTyp typ) {
		this.gueltigAb = gueltigAb;
		this.typ = typ;
	}

	public Zeitkarte(Strecke strecke, Date gueltigAb, ZeitkartenTyp typ) {
		super(strecke);
		this.gueltigAb = gueltigAb;
		this.typ = typ;
	}

	public Zeitkarte() {}

	public Date getGueltigAb() {
		return gueltigAb;
	}

	public void setGueltigAb(Date gueltigAb) {
		this.gueltigAb = gueltigAb;
	}

	public ZeitkartenTyp getTyp() {
		return typ;
	}

	public void setTyp(ZeitkartenTyp typ) {
		this.typ = typ;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Zeitkarte zeitkarte = (Zeitkarte) o;
		return Objects.equals(gueltigAb, zeitkarte.gueltigAb) &&
				typ == zeitkarte.typ;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), gueltigAb, typ);
	}
}

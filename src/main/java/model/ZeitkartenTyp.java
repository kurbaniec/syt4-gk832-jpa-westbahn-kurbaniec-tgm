package model;

public enum ZeitkartenTyp {

	WOCHENKARTE(0),

	MONATSKARTE(1),

	JAHRESKARTE(2);

	private final int value;

	private ZeitkartenTyp(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}

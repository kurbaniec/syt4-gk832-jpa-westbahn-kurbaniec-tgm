package model;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.Arrays;
import java.util.Objects;

@Entity
@NamedQueries({
	@NamedQuery(name="Benutzer.getAll",
			query="SELECT b from Benutzer b"),
	@NamedQuery(name="Benutzer.getEmail",
			query="SELECT b.eMail from Benutzer b"),
	@NamedQuery(name="Benutzer.getReservierungen",
			query = "SELECT r from Reservierung r where r.benutzer.eMail = :email"),
	// Enum-Wert nicht vom Enum-internet Wert abhängig sondern von Position
	@NamedQuery(name="Benutzer.getMonatskarten",
			query = "SELECT b from Benutzer b inner join b.tickets t where t.class = Zeitkarte and t.typ = 1")
})
public class Benutzer {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long ID;

	private String vorName;

	private String nachName;

	@Email
	@Pattern(regexp ="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.(?:[a-zA-Z]{2,6})$", message="Invalid E-Mail")
	@Column(unique = true)
	private String eMail;

	private String passwort;

	private String smsNummer;

	private Long verbuchtePraemienMeilen;

	@OneToMany(cascade=CascadeType.ALL)
	@OrderColumn(name = "ID")
	@ElementCollection(targetClass = Ticket.class)
	private Ticket[] tickets;

	@OneToMany(cascade=CascadeType.ALL)
	@OrderColumn(name = "ID")
	@ElementCollection(targetClass = Reservierung.class)
	private Reservierung[] reservierungen;

	public Benutzer(Long ID) {
		this.ID = ID;
	}

	public Benutzer(String vorName, String nachName, String eMail,
					String passwort, String smsNummer, Long verbuchtePraemienMeilen) {
		this.vorName = vorName;
		this.nachName = nachName;
		this.eMail = eMail;
		this.passwort = passwort;
		this.smsNummer = smsNummer;
		this.verbuchtePraemienMeilen = verbuchtePraemienMeilen;
	}

	public Benutzer() {}

	public Long getID() {
		return ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public String getVorName() {
		return vorName;
	}

	public void setVorName(String vorName) {
		this.vorName = vorName;
	}

	public String getNachName() {
		return nachName;
	}

	public void setNachName(String nachName) {
		this.nachName = nachName;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	public String getSmsNummer() {
		return smsNummer;
	}

	public void setSmsNummer(String smsNummer) {
		this.smsNummer = smsNummer;
	}

	public Long getVerbuchtePraemienMeilen() {
		return verbuchtePraemienMeilen;
	}

	public void setVerbuchtePraemienMeilen(Long verbuchtePraemienMeilen) {
		this.verbuchtePraemienMeilen = verbuchtePraemienMeilen;
	}

	public Ticket[] getTickets() {
		return tickets;
	}

	public void setTickets(Ticket[] tickets) {
		this.tickets = tickets;
	}

	public Reservierung[] getReservierungen() {
		return reservierungen;
	}

	public void setReservierungen(Reservierung[] reservierungen) {
		this.reservierungen = reservierungen;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Benutzer benutzer = (Benutzer) o;
		return Objects.equals(ID, benutzer.ID) &&
				Objects.equals(vorName, benutzer.vorName) &&
				Objects.equals(nachName, benutzer.nachName) &&
				Objects.equals(eMail, benutzer.eMail) &&
				Objects.equals(passwort, benutzer.passwort) &&
				Objects.equals(smsNummer, benutzer.smsNummer) &&
				Objects.equals(verbuchtePraemienMeilen, benutzer.verbuchtePraemienMeilen) &&
				Objects.equals(tickets, benutzer.tickets) &&
				Arrays.equals(reservierungen, benutzer.reservierungen);
	}

	@Override
	public int hashCode() {
		int result = Objects.hash(ID, vorName, nachName, eMail, passwort, smsNummer, verbuchtePraemienMeilen, tickets);
		result = 31 * result + Arrays.hashCode(reservierungen);
		return result;
	}

	@Override
	public String toString() {
		return "Benutzer{" +
				"ID=" + ID +
				", vorName='" + vorName + '\'' +
				", nachName='" + nachName + '\'' +
				", eMail='" + eMail + '\'' +
				", passwort='" + passwort + '\'' +
				", smsNummer='" + smsNummer + '\'' +
				", verbuchtePraemienMeilen=" + verbuchtePraemienMeilen +
				", tickets=" + Arrays.toString(tickets) +
				", reservierungen=" + Arrays.toString(reservierungen) +
				'}';
	}
}

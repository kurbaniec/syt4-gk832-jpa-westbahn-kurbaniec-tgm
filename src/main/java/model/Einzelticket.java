package model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQueries({@NamedQuery(name="Einzelticket.getAll",query="SELECT b from Einzelticket b")})
public class Einzelticket extends Ticket {

	private TicketOption ticketOption;

	public Einzelticket() {}

	public Einzelticket(TicketOption ticketOption) {
		this.ticketOption = ticketOption;
	}

	public TicketOption getTicketOption() {
		return ticketOption;
	}

	public void setTicketOption(TicketOption ticketOption) {
		this.ticketOption = ticketOption;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Einzelticket that = (Einzelticket) o;
		return ticketOption == that.ticketOption;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ticketOption);
	}
}

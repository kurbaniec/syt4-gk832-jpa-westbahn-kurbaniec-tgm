package model;

import javax.persistence.*;
import javax.validation.constraints.Future;
import java.util.Date;
import java.util.Objects;

@Entity
@NamedQueries({@NamedQuery(name="Sonderangebot.getAll",query="SELECT b from Sonderangebot b")})
public class Sonderangebot {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long ID;

	private int kontingent = 999;

	@Future
	private Date startZeit;

	private int dauer = 12;

	private float preisNachlass = 0.5f;

	@OneToOne
	private Ticket tickets;

	public Sonderangebot(Long ID) {
		this.ID = ID;
	}

	public Sonderangebot() {}

	public Long getID() {
		return ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public int getKontingent() {
		return kontingent;
	}

	public void setKontingent(int kontingent) {
		this.kontingent = kontingent;
	}

	public Date getStartZeit() {
		return startZeit;
	}

	public void setStartZeit(Date startZeit) {
		this.startZeit = startZeit;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

	public float getPreisNachlass() {
		return preisNachlass;
	}

	public void setPreisNachlass(float preisNachlass) {
		this.preisNachlass = preisNachlass;
	}

	public Ticket getTickets() {
		return tickets;
	}

	public void setTickets(Ticket tickets) {
		this.tickets = tickets;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Sonderangebot that = (Sonderangebot) o;
		return kontingent == that.kontingent &&
				dauer == that.dauer &&
				Float.compare(that.preisNachlass, preisNachlass) == 0 &&
				Objects.equals(ID, that.ID) &&
				Objects.equals(startZeit, that.startZeit) &&
				Objects.equals(tickets, that.tickets);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ID, kontingent, startZeit, dauer, preisNachlass, tickets);
	}
}

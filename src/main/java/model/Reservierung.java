package model;

import javax.persistence.*;

import java.sql.Date;
import java.util.Objects;

@Entity
@NamedQueries({@NamedQuery(name="Reservierung.getAll",query="SELECT b from Reservierung b")})
public class Reservierung {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long ID;

	private Date datum;

	private int praemienMeilenBonus = 15;

	private int preis = 150;

	private StatusInfo status;

	@ManyToOne
	private Zug zug;

	@ManyToOne
	private Strecke strecke;

	@ManyToOne
	private Benutzer benutzer;

	@Transient
	private Zahlung zahlung;

	public Reservierung(Long ID) {
		this.ID = ID;
	}

	public Reservierung() {}

	public Reservierung(Date datum, Zug zug, Strecke strecke, Benutzer benutzer) {
		this.datum = datum;
		this.zug = zug;
		this.strecke = strecke;
		this.benutzer = benutzer;
	}

	public Reservierung(Date datum, Strecke strecke, Benutzer benutzer) {
		this.datum = datum;
		this.strecke = strecke;
		this.benutzer = benutzer;
	}

	public Reservierung(Date datum, Zug zug, Benutzer benutzer) {
		this.datum = datum;
		this.zug = zug;
		this.benutzer = benutzer;
	}

	public Reservierung(Date datum) {
		this.datum = datum;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public int getPraemienMeilenBonus() {
		return praemienMeilenBonus;
	}

	public void setPraemienMeilenBonus(int praemienMeilenBonus) {
		this.praemienMeilenBonus = praemienMeilenBonus;
	}

	public int getPreis() {
		return preis;
	}

	public void setPreis(int preis) {
		this.preis = preis;
	}

	public StatusInfo getStatus() {
		return status;
	}

	public void setStatus(StatusInfo status) {
		this.status = status;
	}

	public Zug getZug() {
		return zug;
	}

	public void setZug(Zug zug) {
		this.zug = zug;
	}

	public Strecke getStrecke() {
		return strecke;
	}

	public void setStrecke(Strecke strecke) {
		this.strecke = strecke;
	}

	public Benutzer getBenutzer() {
		return benutzer;
	}

	public void setBenutzer(Benutzer benutzer) {
		this.benutzer = benutzer;
	}

	public Zahlung getZahlung() {
		return zahlung;
	}

	public void setZahlung(Zahlung zahlung) {
		this.zahlung = zahlung;
	}

	@Override
	public String toString() {
		return "Reservierung{" +
				"ID=" + ID +
				", datum=" + datum +
				", praemienMeilenBonus=" + praemienMeilenBonus +
				", preis=" + preis +
				", status=" + status +
				", zug=" + zug +
				", strecke=" + strecke +
				", benutzer=" + benutzer +
				", zahlung=" + zahlung +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Reservierung that = (Reservierung) o;
		return praemienMeilenBonus == that.praemienMeilenBonus &&
				preis == that.preis &&
				Objects.equals(ID, that.ID) &&
				Objects.equals(datum, that.datum) &&
				status == that.status &&
				Objects.equals(zug, that.zug) &&
				Objects.equals(strecke, that.strecke) &&
				Objects.equals(benutzer, that.benutzer) &&
				Objects.equals(zahlung, that.zahlung);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ID, datum, praemienMeilenBonus, preis, status, zug, strecke, benutzer, zahlung);
	}
}

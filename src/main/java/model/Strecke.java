package model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQueries({
	@NamedQuery(name="Strecke.getAll",
			query="SELECT b from Strecke b"),
	@NamedQuery(name="Strecke.getNoReservierungen",
			query="select t from Ticket t where t.strecke not in (select r.strecke from Reservierung r)")
})
public class Strecke {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long ID;

	@OneToOne
	private Bahnhof start;

	@OneToOne
	private Bahnhof ende;

	public Strecke(Long ID) {
		this.ID = ID;
	}

	public Strecke() {}

	public Strecke(Bahnhof start, Bahnhof ende) {
		this.start = start;
		this.ende = ende;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public Bahnhof getStart() {
		return start;
	}

	public void setStart(Bahnhof start) {
		this.start = start;
	}


	public Bahnhof getEnde() {
		return ende;
	}

	public void setEnde(Bahnhof ende) {
		this.ende = ende;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Strecke strecke = (Strecke) o;
		return Objects.equals(ID, strecke.ID) &&
				Objects.equals(start, strecke.start) &&
				Objects.equals(ende, strecke.ende);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ID, start, ende);
	}
}

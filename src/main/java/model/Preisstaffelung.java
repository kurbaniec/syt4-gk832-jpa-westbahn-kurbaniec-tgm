package model;

import java.util.Objects;

public class Preisstaffelung {

	private static Long serialVersionUID;

	private float grossGepaeck = 1.02f;

	private float fahrrad = 1.05f;

	private int zeitkarteWoche = 8;

	private int zeitkarteMonat = 25;

	private int zeitkarteJahr = 250;

	private static Preisstaffelung instance;

	public static Preisstaffelung getInstance() {
		return null;
	}

	private Preisstaffelung() {

	}

	public Preisstaffelung(float grossGepaeck) {
		this.grossGepaeck = grossGepaeck;
	}

	public static Long getSerialVersionUID() {
		return serialVersionUID;
	}

	public static void setSerialVersionUID(Long serialVersionUID) {
		Preisstaffelung.serialVersionUID = serialVersionUID;
	}

	public float getGrossGepaeck() {
		return grossGepaeck;
	}

	public void setGrossGepaeck(float grossGepaeck) {
		this.grossGepaeck = grossGepaeck;
	}

	public float getFahrrad() {
		return fahrrad;
	}

	public void setFahrrad(float fahrrad) {
		this.fahrrad = fahrrad;
	}

	public int getZeitkarteWoche() {
		return zeitkarteWoche;
	}

	public void setZeitkarteWoche(int zeitkarteWoche) {
		this.zeitkarteWoche = zeitkarteWoche;
	}

	public int getZeitkarteMonat() {
		return zeitkarteMonat;
	}

	public void setZeitkarteMonat(int zeitkarteMonat) {
		this.zeitkarteMonat = zeitkarteMonat;
	}

	public int getZeitkarteJahr() {
		return zeitkarteJahr;
	}

	public void setZeitkarteJahr(int zeitkarteJahr) {
		this.zeitkarteJahr = zeitkarteJahr;
	}

	public static void setInstance(Preisstaffelung instance) {
		Preisstaffelung.instance = instance;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Preisstaffelung that = (Preisstaffelung) o;
		return Float.compare(that.grossGepaeck, grossGepaeck) == 0 &&
				Float.compare(that.fahrrad, fahrrad) == 0 &&
				zeitkarteWoche == that.zeitkarteWoche &&
				zeitkarteMonat == that.zeitkarteMonat &&
				zeitkarteJahr == that.zeitkarteJahr;
	}

	@Override
	public int hashCode() {
		return Objects.hash(grossGepaeck, fahrrad, zeitkarteWoche, zeitkarteMonat, zeitkarteJahr);
	}
}

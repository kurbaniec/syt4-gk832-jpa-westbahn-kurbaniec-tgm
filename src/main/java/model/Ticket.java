package model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQueries({@NamedQuery(name="Ticket.getAll",query="SELECT b from Ticket b")})
public abstract class Ticket {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected Long ID;

	@ManyToOne
	protected Strecke strecke;

	@Transient
	protected Zahlung zahlung;

	public Ticket() {}

	public Ticket(Long ID) {
		this.ID = ID;
	}

	public Ticket(Strecke strecke) {
		this.strecke = strecke;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long ID) {
		this.ID = ID;
	}

	public Strecke getStrecke() {
		return strecke;
	}

	public void setStrecke(Strecke strecke) {
		this.strecke = strecke;
	}

	public Zahlung getZahlung() {
		return zahlung;
	}

	public void setZahlung(Zahlung zahlung) {
		this.zahlung = zahlung;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Ticket ticket = (Ticket) o;
		return Objects.equals(ID, ticket.ID) &&
				Objects.equals(strecke, ticket.strecke) &&
				Objects.equals(zahlung, ticket.zahlung);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ID, strecke, zahlung);
	}

	@Override
	public String toString() {
		return "Ticket{" +
				"ID=" + ID +
				", strecke=" + strecke +
				", zahlung=" + zahlung +
				'}';
	}
}

# "Komponentenbasierte Programmierung"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Voraussetzung für die die Übung ist Java 8. Versionen 10 und 11 funktionieren nicht.

### Task 1

Zuerst wurden fehlende Code-Teile hinzugefügt:

Main.class:
```java
@PersistenceUnit(unitName = "westbahn")
private static EntityManagerFactory sessionFactory;
```

persistence.xml:
```XML
<persistence-unit name="westbahn">
    <description> Hibernate JPA Configuration Example</description>
    <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
      <class>model.Bahnhof</class>
    <properties>
```

In JPA regelt die persistence.xml die generelle Konfiguration. Unter anderem werden folgenden Punkte dadurch geregelt:

* Der Name der *Persistence Unit* (definiert die Klassen die von einem *EntityManager* ge-managed werden soll, also alle Klassen die einem  Datenspeicher zugeteilt werden sollen)
* Welche [Java-] Klassen persistiert werden sollen
* Welche *Provider* genutzt werden sollen (JPA definiert eine Schnittstelle die durch *Provider* wie Hibernate implementiert wird)
* Welche Datenbank benutzt werden soll (+ Konfiguration dieser)

Als nächstes wurde die Klassen mittels Astah-UML exportiert. Dazu geht man auf den Reiter `Tools` -> `Export Java`. Achtung nicht Bahnhof überschreiben.

Danach wurden fehlende Konstruktoren (Default-Konstruktoren), Setter- und Getter-Methode mithilfe der IDE generiert.

Jetzt müssen alle Klassen nach dem Vorbild von `Bahnhof` als Entitäten gemappt werden, wobei alle bis auf Reservierungen Annotationen verwenden sollen. Bei den Annotation muss man auf die korrekten Beziehungen der Attribute achten, also ob z.B. `@ManyToOne` oder `@OneToMany` benutzt werden soll. Deshalb schaut man sich das UML-Diagramm zur Aufgabe sorgfältig an. Die Klasse Reservierung setzt auf klassisches XML-Mapping und nicht auf Annotations.

```xml
...
<persistence-unit name="westbahn">
    <description> Hibernate JPA Configuration Example</description>
    <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>

      <class>model.Strecke</class>
      <class>model.Benutzer</class>
      <class>model.Zug</class>
      <class>model.Reservierung</class>

      <class>model.Bahnhof</class>
      <class>model.Ticket</class>
      <class>model.Zahlung</class>

      <class>model.Einzelticket</class>
      <class>model.Sonderangebot</class>
      <class>model.Zeitkarte</class>

    <properties>	
    ...
```

Für Reservierung muss man in der `persistence.xml` den Pfad zum XML-Mapping-File hinzufügen:

```xml
...   <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
<mapping-file>/META-INF/reservierung.xml</mapping-file>
<class>model.Strecke</class>
...
```

Das Mapping File `reservierung.xml` für die Klasse `Reservierung` sieht so aus:

```xml
<?xml version = "1.0" encoding = "utf-8"?>
<!DOCTYPE hibernate-mapping PUBLIC
        "-//Hibernate/Hibernate Mapping DTD//EN"
        "http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd">

<hibernate-mapping>
    <class name = "model.Reservierung" table = "EMPLOYEE">

        <meta attribute = "class-description">
            This class contains the employee detail.
        </meta>

        <id name = "ID" type = "long" column = "id">
            <generator class="native"/>
        </id>

        <property name="datum"/>
        <property name="praemienMeilenBonus"/>
        <property name="preis"/>
        <property name="status"/>
        <one-to-one name="zug"/>
        <one-to-one name="strecke"/>
        <many-to-one name="benutzer"/>

    </class>
</hibernate-mapping>
```

Mapping-Files können anstatt *Annotations* verwendet werden, sind aber deutlich aufwändiger als diese.

Achtung: Ich machte hier einen Fehler bei der ID, in dem ich beim type *int* anstatt *long* angab. Der dazugehörige Fehler, denn Hibernate dann ausgab, gab keinen Fehler diesbezüglich, sondern deutete auf falsche Setter-Methoden. 

### Task 2

Ich habe zuerst eine eigene Query für das Auslesen von Email-Adressen in der Klasse `Benutzer` erstellt:

```java
@NamedQueries({
      @NamedQuery(name="Benutzer.getAll",query="SELECT b from Benutzer b"),
      @NamedQuery(name="Benutzer.getEmail",query="SELECT b.eMail from Benutzer b")
})
```

**Aufpassen**:   
Ich habe nicht auf die Namen geachtet, es gibt genau **ein**e `@NamedQueries`-Annotation, die mehrere Queries mit je `@NamedQuery` enthalten kann. 

Bevor ich dies gemacht habe, musste ich Nutzer erstellen. Dazu habe ich einfach in der `fillDB`-Methode im `main`-Programm neue Benutzer erstellt und per `em.persist` zur Persistierung freigegeben. Zur Speicherung müssen diese noch geflusht und commited werden. Mit der Zeit folgten weitere Datensätze für andere Objekte.

#### Task 2 a

*Finde alle Reservierungen für einen bestimmten Benutzer, der durch die eMail-Adresse definiert wird.*

```java
@NamedQuery(name="Benutzer.getReservierungen",
      query = "SELECT r from Reservierung r where r.benutzer.eMail = :email"),
```

`:email` steht für einen Parameter, dieser kann dann beim Ausführen so gesetzt werden.

```java
Query q2 = entitymanager.createNamedQuery("Benutzer.getReservierungen"); // set query
q2.setParameter("email", "kurbaniec@student.tgm.ac.at"); // set parameter
List<?> l2 = q2.getResultList(); // execute and get results
```

#### Task 2 b

*Liste alle Benutzer auf, die eine Monatskarte besitzen.*

```java
@NamedQuery(name="Benutzer.getMonatskarten",
      query = "SELECT b from Benutzer b inner join b.tickets t where t.class = Zeitkarte and t.typ = 1")
```

Bei dieser Query hatte ich das Problem, dass ich falsch in der `reservierung.xml` gemappt habe, also anstatt `many-to-one`-Beziehungen, habe ich `one-to-one` angegeben. Außerdem muss dieser Code Teile beachtet werden: `t.class = Zeitkarte and t.typ = 1`

Dadurch wird das Ticket auf die Klasse Zeitkarte gecastet und man kann auf das Attribut typ zugreifen. Dieses ist ein Enum. Bei Enums steht jetzt dieser Wert "1" nicht für einen Value, sondern für die Ordinale also Index.

#### Task 2 c

*Liste alle Tickets für eine bestimmte Strecke aus (durch Anfangs- und Endbahnhof definiert), wo keine Reservierungen durchgeführt wurden.*

```java
@NamedQuery(name="Strecke.getNoReservierungen",
      query="select t from Ticket t where t.strecke not in (select r.strecke from Reservierung r)")
```

Aufgabe wurde mittels Sub-selects und Mengenvergleich gelöst.

### Task 3

#### Task 3 b

*Die eMail des Benutzers soll ein gängiges eMail-Pattern befolgen.*

Eigentlich sollte Hibernate das Email-Attribut mit folgenden Annotations auf Gültigkeit überprüfen und bei Verstoß einen Fehler ausgeben, doch sie funktionieren nicht:

```java
@Email // standard email validator
@Pattern(regexp ="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.(?:[a-zA-Z]{2,6})$", message="Invalid E-Mail") // validation via regex
@Column(unique = true)
private String eMail;
```

Wahrscheinlich liegt es an einen Konfigurationsfehler von meiner Seite als an den Annotations.

#### Task 3 c

*Die Startzeit eines Sonderangebotes kann nicht in der Vergangenheit liegen.*

```java
@Future
private Date startZeit;
```

#### Task 3 d

*Der Name eines Bahnhofs darf nicht kürzer als zwei und nicht länger als 150 Zeichen sein. Sonderzeichen sind bis auf den Bindestrich zu unterbinden.*

```java
@Size(min=2,max=150,message="Bahnhofname muss mindestens 2 und maximal 150 Zeichen lang sein!")
@Pattern(regexp = "([A-Za-z-])+", message="Invalid Name")
@Column(name="name",unique=true)
private String name;
```

## Quellen

* [Hibernate 5.3 Documentation](<http://docs.jboss.org/hibernate/orm/5.3/userguide/html_single/Hibernate_User_Guide.html>)

* [XML-Mapping File](<https://www.tutorialspoint.com/hibernate/hibernate_mapping_files.htm>)

* [HQL Dokumentatiom](<https://docs.jboss.org/hibernate/orm/3.3/reference/en/html/queryhql.html>)

* [HQL Named-Query](<https://stackoverflow.com/questions/39409622/non-repeatable-namedquery/39409774>)

* [HQL Subclass](https://stackoverflow.com/questions/12715145/hql-on-subclass-property)

* [Persistence Unit Allgemeines](https://thoughts-on-java.org/jpa-persistence-xml/)

* [Email-Regex](https://stackoverflow.com/questions/45101329/email-not-working-but-pattern-does-in-hibernate)

* [JPA-Constraints](http://www.thejavageek.com/2014/05/24/jpa-constraints/)

  